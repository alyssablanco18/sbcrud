package com.codebound.sbcrud.repos;

import com.codebound.sbcrud.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    // the following method is equivalent to the built in 'getOne' method
    @Query("from Employee e where e.id like ?1")
    Employee getEmployeeById(long id);
}
