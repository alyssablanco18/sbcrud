package com.codebound.sbcrud.controllers;

import com.codebound.sbcrud.models.Employee;
import com.codebound.sbcrud.repos.EmployeeRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class EmployeeController {

    // spring boots dependency injection
    private final EmployeeRepository employeeRepo;

    // constructor
    public EmployeeController(EmployeeRepository employeeRepo) {
        this.employeeRepo = employeeRepo;
    }

    // get all the employees and display them on books/index view
    @GetMapping("/employees")
    public String showEmployees(Model model) {
        List<Employee> employeeList = employeeRepo.findAll();
        model.addAttribute("noEmployeesFound", employeeList.size() == 0);
        model.addAttribute("employees",employeeList);
        return "Employees/index"; // name of the view
    }

    // add a employee form
    @GetMapping("/employees/create")
    public String showForm(Model model) {
        model.addAttribute("newEmployee",new Employee());
        return "Employees/create";
    }

    @PostMapping("/employees/create")
    public String createEmployee(@ModelAttribute Employee employeeToCreate) {
        // save the employeeToCreate parameter
        employeeRepo.save(employeeToCreate);
        // redirect the user to the list of employees
        return "redirect:/employees/";
    }

    // controller method to display an individual employee
    @GetMapping("/employees/{id}")
    public String showEmployee(@PathVariable long id, Model model) {
        Employee employee = employeeRepo.getOne(id); // get an object of employee based on id

        model.addAttribute("showEmployee", employee);

        return "Employees/show";
    }

    // controller method that will allow the user to edit
    @GetMapping("/employees/{id}/edit")
    public String showEdit(@PathVariable long id, Model model) {
        // find the employee with the passed in id
        Employee employeeToEdit = employeeRepo.getOne(id);
        model.addAttribute("editEmployee",employeeToEdit);

        return "Employees/edit";
    }

    @PostMapping("/employees/{id}/edit")
    public String updateEmployee(@PathVariable long id,
                                 @RequestParam(name = "firstName") String firstName,
                                 @RequestParam(name = "lastName") String lastName,
                                 @RequestParam(name = "number") int number,
                                 @RequestParam(name = "userName") String userName,
                                 @RequestParam(name = "email") String email)
    {
        // find the employee with the passed in id
        Employee foundEmployee = employeeRepo.getEmployeeById(id);

        // update the employee
        foundEmployee.setFirstName(firstName);
        foundEmployee.setLastName(lastName);
        foundEmployee.setNumber(number);
        foundEmployee.setUserName(userName);
        foundEmployee.setEmail(email);

        // save the new employee changes
        employeeRepo.save(foundEmployee);

        // redirect the user to the url that contains the list of employees
        return "redirect:/employees/";
    }

    // controller method that will allow the user to delete objects of our class
    @PostMapping("/employees/{id}/delete")
    public String delete(@PathVariable long id) {
        // access the deleteById method from the repository
        employeeRepo.deleteById(id);

        // redirect the user to the url containing the list of ads
        return "redirect:/employees/";
    }
}
